# HTML Elements with Template Method pattern
class HTMLElement:
    def on_created(self):
        print("Element created")

    def on_inserted(self):
        print("Element inserted")

    def on_removed(self):
        print("Element removed")

    def on_styles_applied(self):
        print("Styles applied")

    def on_class_list_applied(self):
        print("Class list applied")

    def on_text_rendered(self):
        print("Text rendered")

    def create(self):
        self.on_created()
        # Additional code for creating the element
        self.on_inserted()

    def remove(self):
        # Additional code for removing the element
        self.on_removed()

    def apply_styles(self):
        # Additional code for applying styles
        self.on_styles_applied()

    def apply_class_list(self):
        # Additional code for applying class list
        self.on_class_list_applied()

    def render_text(self):
        # Additional code for rendering text
        self.on_text_rendered()


# Iterator pattern for HTML Elements
class HTMLElementIterator:
    def __init__(self, root):
        self.root = root
        self.stack = [root]

    def __iter__(self):
        return self

    def __next__(self):
        if not self.stack:
            raise StopIteration

        current = self.stack.pop()
        self.stack.extend(reversed(current.children))
        return current


# Command pattern for HTML Elements
class Command:
    def execute(self):
        pass


class AddElementCommand(Command):
    def __init__(self, parent, element):
        self.parent = parent
        self.element = element

    def execute(self):
        self.parent.add_child(self.element)


class RemoveElementCommand(Command):
    def __init__(self, parent, element):
        self.parent = parent
        self.element = element

    def execute(self):
        self.parent.remove_child(self.element)


# State pattern for HTML Elements
class ElementState:
    def handle(self, element):
        pass


class CreatedState(ElementState):
    def handle(self, element):
        element.create()


class RemovedState(ElementState):
    def handle(self, element):
        element.remove()


class Element:
    def __init__(self):
        self.state = CreatedState()

    def change_state(self, state):
        self.state = state

    def handle(self):
        self.state.handle(self)

    def create(self):
        print("Element created")

    def remove(self):
        print("Element removed")


# Visitor pattern for HTML Elements
class ElementVisitor:
    def visit(self, element):
        pass


class RenderVisitor(ElementVisitor):
    def visit(self, element):
        element.render()


class Element:
    def accept(self, visitor):
        visitor.visit(self)

    def render(self):
        print("Element rendered")


# Example usage
if __name__ == "__main__":
    # Using Template Method
    element = HTMLElement()
    element.create()
    element.apply_styles()

    # Using Iterator
    root = HTMLElement()  # Assuming HTMLElement has children
    iterator = HTMLElementIterator(root)
    for elem in iterator:
        print(elem)
